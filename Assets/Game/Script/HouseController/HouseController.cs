﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseController : MonoBehaviour
{

	[Header("Ground Floor References")]
	[SerializeField]
	private Light[] _lights;
	[SerializeField] private GroundFloorController groundFloorController;
	[Header("House References")]
	private TopFloorController _currentTopFloor;

	[Range(0.05f, 1)]
	[SerializeField]
	private float _minScalePossible = 0.1f;
	[SerializeField] private FloorScript _middlePrefab;
	[SerializeField] private TopFloorController _topPrefab;

	private InputController _inputController;
	private QuizController _quizController;

	private Camera userCamera;
	public bool IsEnableHouseToTap { get; set; }

	private List<FloorScript> _middleFloorsStack;
	private FloorScript GetLastFloor(bool takeFromStack = false)
	{
		var lastItem = _middleFloorsStack[_middleFloorsStack.Count - 1];
		if (takeFromStack)
		{
			_middleFloorsStack.Remove(lastItem);
		}

		return lastItem;
	}

	public void InitHouse(Camera userARCamera)
	{
		userCamera = userARCamera;
		IsEnableHouseToTap = true;

		_middleFloorsStack = new List<FloorScript>();
		_middleFloorsStack.Add(groundFloorController);

		_currentTopFloor = Instantiate(_topPrefab, Vector3.zero, transform.rotation, transform);
		UpdateTopFloorPosition();

		_currentTopFloor.InitFloor(userARCamera);
		groundFloorController.InitFloor(userARCamera);

		lightingStateAmount = _lights.Length + 1;

		_inputController = FindObjectOfType<InputController>();

		_quizController = FindObjectOfType<QuizController>();
	}

	public void DestroyHouse()
	{

	}

	#region Basic Control
	public void OnMovementBegan()
	{
		foreach (var item in _middleFloorsStack)
		{
			item.FloorMaterial.color = Color.green;
		}
		_currentTopFloor.FloorMaterial.color = Color.green;
	}

	public void OnMovementEnded()
	{
		foreach (var item in _middleFloorsStack)
		{
			item.ResetMaterialColor();
		}
		_currentTopFloor.ResetMaterialColor();
	}

	#endregion

	#region Additional Floors
	public void AddAdditionalFloor()
	{
		Vector3 targetPosMiddleFloor = new Vector3(
										   GetLastFloor().NextFloorBottom.position.x,
										   GetLastFloor().NextFloorBottom.position.y,
										   GetLastFloor().NextFloorBottom.position.z);
		var midBlock = Instantiate(_middlePrefab, targetPosMiddleFloor, transform.rotation, transform);
		_middleFloorsStack.Add(midBlock);

		UpdateTopFloorPosition();
	}

	public void RemoveAdditionalFloor()
	{
		if (_middleFloorsStack.Count > 1)
		{
			Destroy(GetLastFloor(true).gameObject);

			UpdateTopFloorPosition();
		}
	}
	#endregion

	private void UpdateTopFloorPosition()
	{
		_currentTopFloor.transform.position = new Vector3(
			GetLastFloor().NextFloorBottom.position.x,
			GetLastFloor().NextFloorBottom.position.y,
			GetLastFloor().NextFloorBottom.position.z);
	}
	#region Main Controlls
	public void OnTapOTop()
	{
		if (_quizController != null)
		{
			_quizController.OnItemHasBeenTapped(QuizController.Parts.Top);
		}
		else
		{
			_currentTopFloor.ShowPopup();
		}
	}

	public void OnTapOnBottom()
	{
		if (_quizController != null)
		{
			_quizController.OnItemHasBeenTapped(QuizController.Parts.Ground);
		}
		else
		{
			groundFloorController.ShowPopup();
		}
	}

	private int lightingStateAmount, currentLightingState = -1;

	public void ChangeLightOnEntrance()
	{
		//send to quiz result
		if (_quizController != null)
		{
			_quizController.OnItemHasBeenTapped(QuizController.Parts.Entry);
		}



		//light logic
		foreach (var item in _lights)
		{
			item.gameObject.SetActive(false);
		}

		++currentLightingState;

		currentLightingState = currentLightingState % lightingStateAmount;
		if (currentLightingState == lightingStateAmount - 1)
		{
			return;
		}
		else
		{
			_lights[currentLightingState].gameObject.SetActive(true);
		}

	}

	public void ScaleOn(Vector3 targetScale)
	{
		if (targetScale.x >= _minScalePossible && targetScale.y >= _minScalePossible && targetScale.z >= _minScalePossible)
		{
			transform.localScale = targetScale;
		}
	}

	#endregion

	private void Update()
	{
		var touchPos = GetTouchPos();
		if (touchPos != Vector2.zero)
		{
			if (IsEnableHouseToTap)
			{
				Ray ray = userCamera.ScreenPointToRay(touchPos);
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit))
				{
					if (hit.transform.tag == "Ground Floor")
					{
						OnTapOnBottom();
					}
					else if (hit.transform.tag == "Entrance")
					{
						ChangeLightOnEntrance();
					}
					else if (hit.transform.tag == "Top Floor")
					{
						OnTapOTop();
					}
				}
			}
			else
			{
				IsEnableHouseToTap = true;
				_inputController.ResetTouchBools();
			}
		}

	}

	private Vector2 GetTouchPos()
	{
		Touch touch;
		if (Input.touchCount == 1)
		{
			touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Ended && touch.phase == TouchPhase.Stationary)
			{
				return touch.position;
			}
		}


		if (Input.GetMouseButtonUp(0))
		{
			return Input.mousePosition;
		}

		return Vector2.zero;
	}
}
