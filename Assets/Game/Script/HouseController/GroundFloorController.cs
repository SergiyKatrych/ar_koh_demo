﻿using System;
using System.Collections;
using System.Collections.Generic;

//using UnityEditor;
using UnityEngine;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;

public class GroundFloorController : FloorScript
{
	[Header("Canvas fields")]
	[SerializeField] private string emailFrom = "serhii.katrych@gmail.com";
	[SerializeField] private string emailUsername = "Serhii.Katrych";
	[SerializeField] private string emailUserPassword = "hhomsrsqhjzdwdjz";

	[SerializeField] private string emailWhom = "hello@tsukat.com";
	[SerializeField] private const string emailSubject = "Katrych test task";
	[SerializeField] private const string emailBody = "Hello second plan";
	[SerializeField] private string emailAttachementPath = "Resources/screenshot.png";

	private string localPath;

	[SerializeField] private Canvas _popup;
	private RectTransform _popupPanel;
	private Camera userCamera;

	internal void InitFloor(Camera userARCamera)
	{
		if (userARCamera != null)
		{
			userCamera = userARCamera;
		}
	}

	void Start()
	{
		localPath = Application.persistentDataPath + "/stored_screenshot.png";
		_popupPanel = _popup.transform.GetChild(0).GetComponent<RectTransform>();
		if (userCamera == null)
		{
			userCamera = Camera.main;
		}
	}

	private void Update()
	{
		if (_popup.gameObject.activeSelf)
		{
			var targetPos = GetScreenPosition(transform.parent);

			_popupPanel.anchoredPosition = targetPos;
		}
	}


	public void ShowPopup()
	{
		
		if (_popup.gameObject.activeSelf)
		{
			_popup.gameObject.SetActive(false);
		}
		else
		{
			_popup.renderMode = RenderMode.ScreenSpaceOverlay;
			_popup.worldCamera = userCamera;
			var targetPos = GetScreenPosition(transform.parent);

			_popupPanel.localPosition = targetPos;
			_popup.gameObject.SetActive(true);
		}
	}

	public  Vector3 GetScreenPosition(Transform objTransform)
	{
		Vector3 pos;

		float x = userCamera.WorldToScreenPoint(objTransform.position).x - Screen.width * 0.75f;
		float y = userCamera.WorldToScreenPoint(objTransform.position).y - Screen.height * 0.5f;
		pos = new Vector3(x, y);    
		return pos;    
	}

	public void SendEmail()
	{
		Application.OpenURL(
			"mailto:" + emailWhom +
			"?subject=" + emailSubject +
			"&body=" + emailBody
			+ "&" + "attachement=" + Application.streamingAssetsPath + "screenshot.png"
		);
		Debug.Log("Sending email on " + emailWhom);
	}

	private string GetImageFullPath()
	{
		string streamingPath = Application.streamingAssetsPath;
		return System.IO.Path.Combine(Application.dataPath + "!assets", emailAttachementPath);
	}

	private void ShowAllFiles(string path)
	{
		var info = new DirectoryInfo(path);
		foreach (var item in info.GetFiles())
		{
			Debug.Log("file " + item.FullName);
		}
	}


	public void SendHideEmailMethod()
	{
		StartCoroutine(SendHideEmail());
	}

	public IEnumerator SendHideEmail()
	{
		
		MailMessage mail = new MailMessage();
		mail.From = new MailAddress(emailFrom);
		mail.To.Add(emailWhom);
		mail.Subject = emailSubject;
		mail.Body = emailBody;

		yield return Downloader();

		System.Net.Mail.Attachment attachment;
		attachment = new System.Net.Mail.Attachment(localPath);
		mail.Attachments.Add(attachment);


		Debug.Log("Connecting to SMTP server");
		SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
		smtpServer.UseDefaultCredentials = false;
		smtpServer.Credentials = new System.Net.NetworkCredential(emailUsername, emailUserPassword) as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return true;
		};
		Debug.Log("Sending message");
		smtpServer.Send(mail);

		yield return null;
	}

	IEnumerator Downloader()
	{
		WWW www;
		#if UNITY_ANDROID && !UNITY_EDITOR //For running in Android
		www = new WWW ("jar:file://" + Application.dataPath + "!/assets/screenshot.png");
		#endif
		#if UNITY_EDITOR // For running in Unity
		www = new WWW("file://" + Application.streamingAssetsPath + "/" + emailAttachementPath);
		#endif
		yield return www; //will wait until the download finishes
		if (www.isDone == true)
		{
			yield return SaveFile(www);
		}
	}

	IEnumerator SaveFile(WWW www)
	{
		if (File.Exists(localPath))
		{
			File.Delete(localPath);
		}
		var file = new FileStream(localPath, FileMode.Create);

		System.IO.BinaryWriter binary;
		binary = new BinaryWriter(file);
		binary.Write(www.bytes);
		file.Close();
		yield return 0;
	}
}
