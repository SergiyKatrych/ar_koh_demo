﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneNavigationController : MonoBehaviour {

	//AR Scenes
	public void Open_AR_TestTaskScene()
	{
		SceneManager.LoadScene(IDs.Scenes.AR.TestTask);
	}

	public void Open_AR_QuizGameplay()
	{
		SceneManager.LoadScene(IDs.Scenes.AR.Quiz);
	}



	//Emulation Scenes
	public void Open_TestTaskScene()
	{
		SceneManager.LoadScene(IDs.Scenes.Emulation.TestTask);
	}

	public void Open_QuizGameplay()
	{
		SceneManager.LoadScene(IDs.Scenes.Emulation.Quiz);
	}
}
