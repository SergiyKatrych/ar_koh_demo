﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDs
{

	public struct Scenes
	{
		public struct AR
		{
			public const string Quiz = "AR_QuizGameplay";
			public const string TestTask = "AR_TestTask";
		}

		public struct Emulation
		{
			public const string Quiz = "QuizGameplay";
			public const string TestTask = "TestTask";
		}
	}

}
