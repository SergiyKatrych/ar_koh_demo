﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizUIController : MonoBehaviour
{

	public Text questionLabel, questionBodyLabel;
	public GameObject winningPanel;

	public void SetTexts(int questionIndex, int maxQuestions, QuizController.Parts floorEnum)
	{
		questionLabel.text = "Question " + questionIndex+" of "+ maxQuestions;
		questionBodyLabel.text = "Tap on the " + GetFloorName(floorEnum) + " floor";
	}

	internal string GetFloorName(QuizController.Parts parts)
	{
		switch (parts)
		{
			case QuizController.Parts.Entry:
				return "Entry";
			case QuizController.Parts.Ground:
				return "Ground";
			case QuizController.Parts.Top:
				return "Top";
			default:
				break;
		}

		return "";
	}

	internal void ShowCongratsPanel()
	{
		winningPanel.SetActive(true);
	}

	public void RestartQuiz()
	{
		FindObjectOfType<QuizController>().ReplayQuiz();
		winningPanel.SetActive(false);
	}
}
