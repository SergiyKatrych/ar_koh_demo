﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizController : MonoBehaviour
{
	public enum Parts
	{
		Entry = 0,
		Ground = 1,
		Top = 2
	}


	public QuizUIController quizQuestionUIPrefab;
	private QuizUIController currentQuizQuestionUI;

	private Stack<Parts> quizQuestions;
	private Parts currentWaitingPart;
	private int currentQuestion = 0;
	private int quiestionsCount;


	// Use this for initialization
	public void Init()
	{
		quizQuestions = new Stack<Parts>();
		quizQuestions.Push(Parts.Top);
		quizQuestions.Push(Parts.Ground);
		quizQuestions.Push(Parts.Entry);
		quizQuestions.Push(Parts.Top);
		quizQuestions.Push(Parts.Entry);

		currentQuestion = 0;
		quiestionsCount = quizQuestions.Count;

		NextQuestion();
	}

	#region Quiz
	public void NextQuestion()
	{
		if (currentQuizQuestionUI == null)
		{
			currentQuizQuestionUI = Instantiate(quizQuestionUIPrefab);
		}

		if (quizQuestions.Count > 0)
		{
			currentWaitingPart = quizQuestions.Pop();
			currentQuizQuestionUI.SetTexts(++currentQuestion, quiestionsCount, currentWaitingPart);
		}
		else
		{
			Win();
		}
	}

	public void OnItemHasBeenTapped(Parts part)
	{
		Debug.Log("on tap "+part+"   waiting "+ currentWaitingPart);
		if (currentWaitingPart == part)
		{
			//Highlilght answer
			NextQuestion();
		}
	}

	public void Win()
	{
		currentQuizQuestionUI.ShowCongratsPanel();
	}

	public void ReplayQuiz()
	{
		Init();
	}
	#endregion
}
